# ldap-authentificator

Nette\DI extension for provide LDAP authenticate via ldap servers on Charles University in Prague

## Install
```sh
$ composer require fsv-dev/ldap-authenticator
```

## Configuration

### config.neon
```php

extensions:
	ldap: Ldap\DI\LdapExtension
	
ldap:
    server: 'ldaps://ldap.com'
    port: 636 // Optional (default 636)
    dn: 'dc=cz'
    user: 'username' 
    password: 'passWorD'
```
